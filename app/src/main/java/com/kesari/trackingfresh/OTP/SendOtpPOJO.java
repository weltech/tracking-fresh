package com.kesari.trackingfresh.OTP;

/**
 * Created by kesari on 01/06/17.
 */

public class SendOtpPOJO {

    String status,message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
