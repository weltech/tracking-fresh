package com.kesari.trackingfresh.ConfirmOrder;

/**
 * Created by kesari on 08/06/17.
 */

public class OrderAddPojo {

    private OrderAddSubPOJO message;

    public OrderAddSubPOJO getMessage() {
        return message;
    }

    public void setMessage(OrderAddSubPOJO message) {
        this.message = message;
    }
}
