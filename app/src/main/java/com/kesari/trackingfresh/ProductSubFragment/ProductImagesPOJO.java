package com.kesari.trackingfresh.ProductSubFragment;

/**
 * Created by kesari on 23/08/17.
 */

public class ProductImagesPOJO {

    private String _id;
    private String url;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
