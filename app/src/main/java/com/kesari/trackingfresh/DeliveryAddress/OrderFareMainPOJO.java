package com.kesari.trackingfresh.DeliveryAddress;

/**
 * Created by kesari on 28/06/17.
 */

public class OrderFareMainPOJO {

    private OrderFareSubPOJO data;

    public OrderFareSubPOJO getData() {
        return data;
    }

    public void setData(OrderFareSubPOJO data) {
        this.data = data;
    }
}
