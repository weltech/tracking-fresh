package com.kesari.trackingfresh.DeliveryAddress.AddDeliveryAddress;

import com.kesari.trackingfresh.DeliveryAddress.AddressPOJO;

/**
 * Created by kesari on 05/06/17.
 */

public class AddAddressPOJO {

    private AddressPOJO address;

    public AddressPOJO getAddress() {
        return address;
    }

    public void setAddress(AddressPOJO address) {
        this.address = address;
    }
}
