package com.kesari.trackingfresh.OrderTracking;

/**
 * Created by kesari on 28/08/17.
 */

public class BikerSocketLivePOJO {

    private BikerSocketDataPojo data;

    public BikerSocketDataPojo getData() {
        return data;
    }

    public void setData(BikerSocketDataPojo data) {
        this.data = data;
    }
}
