package com.kesari.trackingfresh.DashBoard;

/**
 * Created by kesari on 31/05/17.
 */

public class VerifyMobilePOJO {

    String message,status,mobileNo;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
