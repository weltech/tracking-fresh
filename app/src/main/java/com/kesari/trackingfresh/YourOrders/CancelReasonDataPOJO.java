package com.kesari.trackingfresh.YourOrders;

/**
 * Created by kesari on 26/07/17.
 */

public class CancelReasonDataPOJO {

    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
