package com.kesari.trackingfresh.YourOrders.RepeatOrder;

/**
 * Created by kesari on 10/10/17.
 */

public class RepeatOrderMainPojo {

    private RepeatOrderSubPojo data;

    public RepeatOrderSubPojo getData() {
        return data;
    }

    public void setData(RepeatOrderSubPojo data) {
        this.data = data;
    }
}
