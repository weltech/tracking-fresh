package com.kesari.trackingfresh.VehicleRoute;

/**
 * Created by kesari on 10/08/17.
 */

public class RouteMainPOJO {

    private RouteData data;

    public RouteData getData() {
        return data;
    }

    public void setData(RouteData data) {
        this.data = data;
    }
}
