package com.kesari.trackingfresh.CheckNearestVehicleAvailability;

/**
 * Created by kesari on 28/06/17.
 */

public class Dist {

    private Location location;

    private String calculated;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getCalculated() {
        return calculated;
    }

    public void setCalculated(String calculated) {
        this.calculated = calculated;
    }
}
