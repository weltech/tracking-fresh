package com.kesari.trackingfresh.ProductMainFragment;

/**
 * Created by kesari on 18/05/17.
 */

public class ProductCategorySubPOJO {

    String _id,categoryName,categoryImage;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }
}
