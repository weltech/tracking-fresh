package com.kesari.trackingfresh.ProductMainFragment;

/**
 * Created by kesari on 23/08/17.
 */

public class SocketLiveMainPOJO {

    private SocketLiveDataPOJO data;

    public SocketLiveDataPOJO getData() {
        return data;
    }

    public void setData(SocketLiveDataPOJO data) {
        this.data = data;
    }
}
