package com.kesari.trackingfresh.VehicleNearestRoute;

/**
 * Created by kesari on 23/08/17.
 */

public class Dist {

    private LocationPOJO location;

    private String calculated;

    public LocationPOJO getLocation() {
        return location;
    }

    public void setLocation(LocationPOJO location) {
        this.location = location;
    }

    public String getCalculated() {
        return calculated;
    }

    public void setCalculated(String calculated) {
        this.calculated = calculated;
    }
}
