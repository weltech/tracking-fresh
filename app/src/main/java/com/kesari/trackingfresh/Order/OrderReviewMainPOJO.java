package com.kesari.trackingfresh.Order;

/**
 * Created by kesari on 13/06/17.
 */

public class OrderReviewMainPOJO {

    private OrderReviewDataPOJO data;

    public OrderReviewDataPOJO getData() {
        return data;
    }

    public void setData(OrderReviewDataPOJO data) {
        this.data = data;
    }
}
